# 👏 RPG Heroes

This i a projct made out of Java. The main usage of the application is to create a RPG hero character.

## ✌️ Description

### 🔥 About the application

RPG Heroes is an application made in Java. You are to insert values to the character and se how the values are increased.

You can pick from three(4) different classes* with different attributes, armor and weapons.

(*) Warrior, Rogue, Mage, Ranger

There is some main features in the application:

* Hero name - You are able to create a new hero with you name.

* Equip weapons and armors to the character if they meet the requirements.

* DPS will adjust after level up and equipment.



### 🤖 Technologies

* Java
* JUnit Idea
* Intellij
* Git Bash

### ⚠ Dependencies

* We recommend to not change the versions in the dependencies. Otherwise, the application might not work.

### ⚡ Installing & Running

#### 🦾 Install 

Follow the steps to clone the application.

1. Clone the repo:
```
git clone https://gitlab.com/noroff1/java-rpg-game.git
``` 



#### 💻 Executing program

How to run the application:
```
Build Project - main.
Run
```

## 😎 Authors

Contributors and contact information

Philip Hjelmberg
[@PhilipHjelmberg](https://gitlab.com/PhilipHjelmberg)

## 🦝 Version History

* 0.1
    * Initial Release

## 🌌 License

This project is fully free for use.
