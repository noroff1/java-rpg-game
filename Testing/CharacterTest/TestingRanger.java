package CharacterTest;


import Heroes.Ranger;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestingRanger {
    @Test

    public void ranger_level_1_return() {
        // AAA - Arrange
        Ranger ranger = new Ranger("Creating new ranger with level 1");
        int expected = 1;

        // AAA - Act
        int actualLevel = ranger.getLevel();

        // AAA - Assert
        assertEquals(expected, actualLevel);
    }
    @Test

    public void ranger_level_up_return() {
        // AAA - Arrange
        Ranger ranger = new Ranger("Creating new ranger and level up to 2");
        int expected = 2;

        // AAA - Act
        ranger.levelUp();
        int actualNewLevel = ranger.getLevel();

        // AAA - Assert
        assertEquals(expected, actualNewLevel);
    }
    @Test

    public void ranger_starting_attribute_return() {
        // AAA - Arrange
        Ranger ranger = new Ranger("Ranger level 1 starting attributes");
        int strExpected = 2;
        int intExpected = 1;
        int dexExpected = 6;

        // AAA - Act
        int strActual = ranger.getAttributes().myAttributes("strength");
        int intActual = ranger.getAttributes().myAttributes("intelligence");
        int dexActual = ranger.getAttributes().myAttributes("dexterity");

        // AAA - Assert
        assertEquals(strExpected, strActual);
        assertEquals(intExpected, intActual);
        assertEquals(dexExpected, dexActual);
    }
    @Test
    public void ranger_attributes_after_level_up_return() {
        // AAA - Arrange
        Ranger ranger = new Ranger("Level up attributes should return: str-3, int-2, dex-10 ");
        int strExpected = 3;
        int intExpected = 2;
        int dexExpected = 10;

        // AAA - Act
        ranger.levelUp();
        int strActual = ranger.getAttributes().myAttributes("strength");
        int intActual = ranger.getAttributes().myAttributes("intelligence");
        int dexActual = ranger.getAttributes().myAttributes("dexterity");

        // AAA - Assert
        assertEquals(strExpected, strActual);
        assertEquals(intExpected, intActual);
        assertEquals(dexExpected, dexActual);
    }


}
