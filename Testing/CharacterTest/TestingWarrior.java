package CharacterTest;


import Heroes.Warrior;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestingWarrior {
    @Test

    public void warrior_level_1_return() {
        // AAA - Arrange
        Warrior warrior = new Warrior("Creating new warrior with level 1");
        int expected = 1;

        // AAA - Act
        int actualLevel = warrior.getLevel();

        // AAA - Assert
        assertEquals(expected, actualLevel);
    }
    @Test

    public void warrior_level_up_return() {
        // AAA - Arrange
        Warrior warrior = new Warrior("Creating new warrior and level up to 2");
        int expected = 2;

        // AAA - Act
        warrior.levelUp();
        int actualNewLevel = warrior.getLevel();

        // AAA - Assert
        assertEquals(expected, actualNewLevel);
    }
    @Test

    public void warrior_starting_attribute_return() {
        // AAA - Arrange
        Warrior warrior = new Warrior("Warrior level 1 starting attributes");
        int strExpected = 5;
        int intExpected = 1;
        int dexExpected = 2;

        // AAA - Act
        int strActual = warrior.getAttributes().myAttributes("strength");
        int intActual = warrior.getAttributes().myAttributes("intelligence");
        int dexActual = warrior.getAttributes().myAttributes("dexterity");

        // AAA - Assert
        assertEquals(strExpected, strActual);
        assertEquals(intExpected, intActual);
        assertEquals(dexExpected, dexActual);
    }
    @Test
    public void warrior_attributes_after_level_up_return() {
        // AAA - Arrange
        Warrior warrior = new Warrior("Level up attributes should return: str-8, int-2, dex-4 ");
        int strExpected = 8;
        int intExpected = 2;
        int dexExpected = 4;

        // AAA - Act
        warrior.levelUp();
        int strActual = warrior.getAttributes().myAttributes("strength");
        int intActual = warrior.getAttributes().myAttributes("intelligence");
        int dexActual = warrior.getAttributes().myAttributes("dexterity");

        // AAA - Assert
        assertEquals(strExpected, strActual);
        assertEquals(intExpected, intActual);
        assertEquals(dexExpected, dexActual);
    }


}
