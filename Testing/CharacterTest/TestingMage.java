package CharacterTest;


import Heroes.Mage;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestingMage {
    @Test

    public void mage_level_1_return() {
        // AAA - Arrange
        Mage mage = new Mage("Creating new mage with level 1");
        int expected = 1;

        // AAA - Act
        int actualLevel = mage.getLevel();

        // AAA - Assert
        assertEquals(expected, actualLevel);
    }
    @Test

    public void mage_level_up_return() {
        // AAA - Arrange
        Mage mage = new Mage("Creating new mage and level up to 2");
        int expected = 2;

        // AAA - Act
        mage.levelUp();
        int actualNewLevel = mage.getLevel();

        // AAA - Assert
        assertEquals(expected, actualNewLevel);
    }
    @Test

    public void mage_starting_attribute_return() {
        // AAA - Arrange
        Mage mage = new Mage("Mage level 1 starting attributes");
        int strExpected = 1;
        int intExpected = 1;
        int dexExpected = 8;

        // AAA - Act
        int strActual = mage.getAttributes().myAttributes("strength");
        int intActual = mage.getAttributes().myAttributes("intelligence");
        int dexActual = mage.getAttributes().myAttributes("dexterity");

        // AAA - Assert
        assertEquals(strExpected, strActual);
        assertEquals(intExpected, intActual);
        assertEquals(dexExpected, dexActual);
    }
    @Test
    public void mage_attributes_after_level_up_return() {
        // AAA - Arrange
        Mage mage = new Mage("Level up attributes should return: str-2, int-2, dex-13 ");
        int strExpected = 2;
        int intExpected = 2;
        int dexExpected = 13;

        // AAA - Act
        mage.levelUp();
        int strActual = mage.getAttributes().myAttributes("strength");
        int intActual = mage.getAttributes().myAttributes("intelligence");
        int dexActual = mage.getAttributes().myAttributes("dexterity");

        // AAA - Assert
        assertEquals(strExpected, strActual);
        assertEquals(intExpected, intActual);
        assertEquals(dexExpected, dexActual);
    }


}
