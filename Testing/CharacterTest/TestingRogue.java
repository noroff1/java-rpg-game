package CharacterTest;


import Heroes.Rogue;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestingRogue {
    @Test

    public void rogue_level_1_return() {
        // AAA - Arrange
        Rogue rogue = new Rogue("Creating new rogue with level 1");
        int expected = 1;

        // AAA - Act
        int actualLevel = rogue.getLevel();

        // AAA - Assert
        assertEquals(expected, actualLevel);
    }
    @Test

    public void rogue_level_up_return() {
        // AAA - Arrange
        Rogue rogue = new Rogue("Creating new rogue and level up to 2");
        int expected = 2;

        // AAA - Act
        rogue.levelUp();
        int actualNewLevel = rogue.getLevel();

        // AAA - Assert
        assertEquals(expected, actualNewLevel);
    }
    @Test

    public void rogue_starting_attribute_return() {
        // AAA - Arrange
        Rogue rogue = new Rogue("Rogue level 1 starting attributes");
        int strExpected = 2;
        int intExpected = 1;
        int dexExpected = 6;

        // AAA - Act
        int strActual = rogue.getAttributes().myAttributes("strength");
        int intActual = rogue.getAttributes().myAttributes("intelligence");
        int dexActual = rogue.getAttributes().myAttributes("dexterity");

        // AAA - Assert
        assertEquals(strExpected, strActual);
        assertEquals(intExpected, intActual);
        assertEquals(dexExpected, dexActual);
    }
    @Test
    public void rogue_attributes_after_level_up_return() {
        // AAA - Arrange
        Rogue rogue = new Rogue("Level up attributes should return: str-3, int-2, dex-8 ");
        int strExpected = 3;
        int intExpected = 2;
        int dexExpected = 8;

        // AAA - Act
        rogue.levelUp();
        int strActual = rogue.getAttributes().myAttributes("strength");
        int intActual = rogue.getAttributes().myAttributes("intelligence");
        int dexActual = rogue.getAttributes().myAttributes("dexterity");

        // AAA - Assert
        assertEquals(strExpected, strActual);
        assertEquals(intExpected, intActual);
        assertEquals(dexExpected, dexActual);
    }


}
