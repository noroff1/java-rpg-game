package Items;

import Enums.Slots;

public abstract class Items {

    protected String name;
    protected int reqLevel;
    protected Slots slots;

    public Items() {
    }

    public Items(String name, int reqLevel, Slots slots) {
        this.name = name;
        this.reqLevel = reqLevel;
        this.slots = slots;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getReqLevel() {
        return reqLevel;
    }

    public void setReqLevel(int reqLevel) {
        this.reqLevel = reqLevel;
    }

    public Slots getSlots() {
        return slots;
    }

    public void setSlots(Slots slots) {
        this.slots = slots;
    }
}
