package Items;

import Enums.Slots;
import Enums.WeaponType;

public class Weapon extends Items {

    protected WeaponType weaponType;
    protected double dmg;
    protected double speed;
    protected double dps;

    public Weapon(String name, int reqLevel, Slots slots, WeaponType weaponType, double dmg, double speed) {
        super(name, reqLevel, slots);
        this.weaponType = weaponType;
        this.dmg = dmg;
        this.speed = speed;
    }

    public WeaponType getWeaponType() {
        return weaponType;
    }

    public void setWeaponType(WeaponType weaponType) {
        this.weaponType = weaponType;
    }

    public double getDmg() {
        return dmg;
    }

    public void setDmg(double dmg) {
        this.dmg = dmg;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public double getDps() {
        return dps;
    }

    public void setDps(double dps) {
        this.dps =  this.dps * this.speed;
    }
}
