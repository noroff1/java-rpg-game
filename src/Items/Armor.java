package Items;

import Enums.ArmorType;
import Enums.Slots;
import Heroes.Attributes;

public class Armor extends Items {

    protected ArmorType armorType;
    protected Attributes attributes;

    public Armor(String name, int reqLevel, Slots slots, ArmorType armorType, Attributes attributes) {
        super(name, reqLevel, slots);
        this.armorType = armorType;
        this.attributes = attributes;
    }

    public ArmorType getArmorType() {
        return armorType;
    }

    public void setArmorType(ArmorType armorType) {
        this.armorType = armorType;
    }

    public Attributes getAttributes() {
        return attributes;
    }

    public void setAttributes(Attributes attributes) {
        this.attributes = attributes;
    }
}
