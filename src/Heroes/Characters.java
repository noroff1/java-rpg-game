package Heroes;


import Enums.Slots;
import Items.Items;
import Items.Armor;
import Items.Weapon;
import java.util.EnumMap;

public abstract class Characters implements Featureable {

  protected String name;
  protected int level;
  protected Attributes attributes;
  protected int baseAttributes;
  protected int totalAttributes;
  protected String attributeType;
  protected EnumMap<Slots, Items> myItems;

    // Constructors
    public Characters() {
    }
    // Setting up character settings by name, level, attributes and items.
    public Characters(String name, int level, int strength, int dexterity, int intelligence) {
        this.name = name;
        this.level = level;
        this.attributes = new Attributes(strength, dexterity, intelligence);
        // Give keys to the slots
        this.myItems = new EnumMap<>(Slots.class);
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public EnumMap<Slots, Items> getMyItems() {
        return myItems;
    }

    public void setMyItems(EnumMap<Slots, Items> myItems) {
        this.myItems = myItems;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel() {
        level++;
    }

    public Attributes getAttributes() {
        return attributes;
    }

    public void setAttributes(Attributes attributes) {
        this.attributes = attributes;
    }

    public void setTotalAttributes(int totalAttributes) {
        this.totalAttributes += totalAttributes;
    }
    public int getTotalAttributes() {
        totalAttributes = baseAttributes;
        setTotalAttributes();
        return totalAttributes;
    }
    public String getAttributeType() {
        return attributeType;
    }

    // Check if it´s possible to equip the weapon and add to slot.
    public void equipWeapon(Weapon weapon) {
        try {
            equipMyWeapon(weapon);
            myItems.put(Slots.WEAPONS, weapon);
        } catch (Exception exc) {
            System.out.println("You cant equip this weapon");
        }
    }

    // Check if it´s possible to equip the armor and add to slot.
    public void equipArmor(Armor armor) {
        try {
            equipMyArmor(armor);
            myItems.put(armor.getSlots(), armor);
        } catch (Exception exc) {
            System.out.println("You armor haven't been equipped");
        }
    }

    // Checks if there is any armor in spot and adds the attribute.
    public void setTotalAttributes() {
        Armor headSpot = (Armor) myItems.get(Slots.HEAD);
        Armor bodySpot = (Armor) myItems.get(Slots.BODY);
        Armor legSpot = (Armor) myItems.get(Slots.LEGS);

        int attributesFromArmor = 0;
        if (headSpot != null) {
            attributesFromArmor += headSpot.getAttributes().myAttributes(getAttributeType());
        }
        if (bodySpot != null) {
            attributesFromArmor += bodySpot.getAttributes().myAttributes(getAttributeType());
        }
        if (legSpot != null) {
            attributesFromArmor += legSpot.getAttributes().myAttributes(getAttributeType());
        }
        setTotalAttributes(attributesFromArmor);
    }

    // Get the DPS
    public double getDps() {
        double dps = 1;
        Weapon activeWeapon = (Weapon) myItems.get(Slots.WEAPONS);
        if (activeWeapon != null) {
            dps = activeWeapon.getDps();
        }
        int totalAttributes = getTotalAttributes();
        return (dps * (1 + (totalAttributes/100)));
    }

    // Print out the statistics
    public void statistics() {
        String stats = "Your hero name: " + getName() + "\n" +
                       "Level: " + getLevel() + "\n" +
                       "Strength: " + this.attributes.strength + "\n" +
                       "Dexterity : " + this.attributes.dexterity + "\n" +
                       "Intelligence: " + this.attributes.intelligence + "\n" +
                       "DPS: " + getDps();
        System.out.println(stats);
    }
}

