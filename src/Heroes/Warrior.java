package Heroes;

import Enums.Slots;
import Exceptions.InvalidArmorException;
import Exceptions.InvalidWeaponException;
import Items.Armor;
import Items.Weapon;

public class Warrior extends Characters {

    // Increment attributes on level up
    @Override
    public void levelUp() {
        this.attributes.strength += 3;
        this.attributes.dexterity += 2;
        this.attributes.intelligence += 1;
        this.baseAttributes += 3;
        this.totalAttributes += 3;
        setLevel();
    }

    // Equip weapon if hero meets the requirements.
    @Override
    public void equipMyWeapon(Weapon weapon) throws InvalidWeaponException {
        if(this.getLevel() >= weapon.getReqLevel() && weapon.getWeaponType().toString().equals("AXES") || weapon.getWeaponType().toString().equals("SWORDS") || weapon.getWeaponType().toString().equals("HAMMERS")){
            System.out.println("You equipped your weapon!");
        } else {
            throw new InvalidWeaponException("Cant equip this weapon");
        }

    }

    // Equip armor if hero meets the requirements.
    @Override
    public void equipMyArmor(Armor armor) throws InvalidArmorException {
        if (armor.getArmorType().toString().equals("PLATE") || armor.getArmorType().toString().equals("MAIL") && this.getLevel() >= armor.getReqLevel()) {
            System.out.println("You equipped your armor!");
        } else {
            throw new InvalidArmorException("Cant equip this weapon");
        }
    }

    // Warrior hero name, level and starting attributes.
    public Warrior(String name) {
        super(name, 1, 5, 2, 1);

    }
}
