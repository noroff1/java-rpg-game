package Heroes;

import Exceptions.InvalidArmorException;
import Exceptions.InvalidWeaponException;
import Items.Armor;
import Items.Weapon;

public interface Featureable {

    void levelUp();
    void equipMyWeapon(Weapon weapon) throws InvalidWeaponException;
    void equipMyArmor(Armor armor) throws InvalidArmorException;
}
