package Heroes;

import Enums.Slots;
import Items.Armor;
import Items.Weapon;

public class Rogue extends Characters {
    // Increment attributes on level up
    @Override
    public void levelUp() {
        this.attributes.strength += 1;
        this.attributes.dexterity += 4;
        this.attributes.intelligence += 1;
        this.baseAttributes += 3;
        this.totalAttributes += 3;
        setLevel();
    }

    // Equip weapon if hero meets the requirements.
    @Override
    public void equipMyWeapon(Weapon weapon) {
        if(this.getLevel() >= weapon.getReqLevel() && weapon.getWeaponType().toString().equals("DAGGERS") || weapon.getWeaponType().toString().equals("SWORDS")){
            myItems.put(Slots.WEAPONS, weapon);
        } else {
            System.out.println("Cant equip that weapon");
        }
    }

    // Equip armor if hero meets the requirements.
    @Override
    public void equipMyArmor(Armor armor) {
        if (this.getLevel() >= armor.getReqLevel() && armor.getArmorType().toString().equals("LEATHER") || armor.getArmorType().toString().equals("MAIL")) {
            myItems.put(armor.getSlots(), armor);
        } else {
            System.out.println("You cant wear this armor");
        }
    }

    // Rogue hero name, level and starting attributes.
    public Rogue(String name) {
        super(name, 1, 2, 6, 1);


    }
}

