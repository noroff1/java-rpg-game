package Heroes;

import Enums.Slots;
import Items.Armor;
import Items.Weapon;

public class Mage extends Characters {

    // Increment attributes on level up
    @Override
    public void levelUp() {
        this.attributes.strength += 1;
        this.attributes.dexterity += 1;
        this.attributes.intelligence += 5;
        this.baseAttributes += 3;
        this.totalAttributes += 3;
        setLevel();
    }

    // Equip weapon if hero meets the requirements.
    @Override
    public void equipMyWeapon(Weapon weapon) {
        if (this.getLevel() >= weapon.getReqLevel() && weapon.getWeaponType().toString().equals("STAFFS") || weapon.getWeaponType().toString().equals("WANDS")) {
            myItems.put(Slots.WEAPONS, weapon);
        } else {
            System.out.println("Cant equip that weapon");
        }
    }

    // Equip armor if hero meets the requirements.
    @Override
    public void equipMyArmor(Armor armor) {
        if (this.getLevel() >= armor.getReqLevel() && armor.getArmorType().toString().equals("CLOTH")) {
            myItems.put(armor.getSlots(), armor);
        } else {
            System.out.println("You cant wear this armor");
        }
    }

    // Mage hero name, level and starting attributes.
    public Mage(String name) {
        super(name, 1, 1, 1, 8);
    }
}

