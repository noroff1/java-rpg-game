package Heroes;

public class Attributes {
    protected int strength;
    protected int dexterity;
    protected int intelligence;

    //
    public Attributes(int strength, int dexterity, int intelligence) {
        this.strength = strength;
        this.dexterity = dexterity;
        this.intelligence = intelligence;
    }

    public int myAttributes(String attribute) {
        if (attribute.equals("strength")) {
            return strength;
        }
        if (attribute.equals("dexterity")) {
            return dexterity;
        }
        if (attribute.equals("intelligence")) {
            return intelligence;
        }
        return 0;
    }

}
