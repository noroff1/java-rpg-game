package philip.rpg.game;

import Enums.ArmorType;
import Enums.Slots;
import Enums.WeaponType;
import Heroes.Attributes;
import Heroes.Warrior;
import Items.Armor;
import Items.Weapon;

public class Main {
    public static void main(String[] args) {


        Warrior warrior = new Warrior("Phil the warrior");
        Weapon weapon = new Weapon("Axe", 3, Slots.WEAPONS, WeaponType.AXES, 10, 1);
        Armor armor = new Armor("Mail", 2, Slots.HEAD, ArmorType.MAIL, new Attributes(5,5,5));
        warrior.levelUp();
        warrior.statistics();
    }
}

